<?php

namespace MicroSymfony\Service;

use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\DefinitionDecorator;
use Symfony\Component\ExpressionLanguage\Expression;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\DependencyInjection\ContainerInterface;

class DefinitionBuilder
{
    private $definition;

    public function __construct(Definition $definition)
    {
        $this->definition = $definition;
    }

    public static function create($class = null, $arguments = array())
    {
        $arguments = self::resolveServices($arguments);
        $definition = new Definition($class, $arguments);

        return new static($definition);
    }

    public static function createFromTemplate($parentId)
    {
        $definition = new DefinitionDecorator($parentId);

        return new static($definition);
    }

    public function setArguments(array $arguments)
    {
        $this->definition->setArguments($this->resolveServices($arguments));
    }

    public function setProperties(array $properties)
    {
        $this->definition->setProperties($this->resolveServices($properties));
    }



    /**
     * Sets this service to be created by a factory function.
     *
     * This callable can be in any callable format:
     *
     *      $d->instantiateFromFactory(array('@my_service', 'methodName'));
     *      $d->instantiateFromFactory('MyClass::methodName');
     *
     * @param mixed $callable
     * @return $this
     */
    public function instantiateFromFactory($callable)
    {
        if (is_string($callable)) {
            if (strpos($callable, ':') !== false && strpos($callable, '::') === false) {
                $parts = explode(':', $callable);
                $this->definition->setFactory(array($this->resolveServices('@'.$parts[0]), $parts[1]));
            } else {
                $this->definition->setFactory($callable);
            }
        } else {
            $this->definition->setFactory(array($this->resolveServices($callable[0]), $callable[1]));
        }

        return $this;
    }

    public function setConfigurator($callable)
    {
        if (is_string($callable)) {
            $this->definition->setConfigurator($callable);
        } else {
            $this->definition->setConfigurator(array($this->resolveServices($callable[0]), $callable[1]));
        }

        return $this;
    }

    public function addMethodCall($method, $arguments = array())
    {
        $this->definition->addMethodCall($method, $this->resolveServices($arguments));

        return $this;
    }

    public function addTag($name, array $attributes = array())
    {
        $this->definition->addTag($name, $attributes);

        return $this;
    }

    public function addEventListenerTag($eventName, $methodName, $priority = null)
    {
        return $this->addTag('kernel.event_listener', array(
            'event' => $eventName,
            'method' => $methodName,
            'priority' => $priority
        ));
    }

    public function addEventSubscriberTag()
    {
        return $this->addTag('kernel.event_subscriber');
    }

    public function getDefinition()
    {
        return $this->definition;
    }

    /**
     * Resolves services.
     *
     * @param string|array $value
     *
     * @return array|string|Reference
     */
    private static function resolveServices($value)
    {
        if (is_array($value)) {
            $value = array_map(array('self', 'resolveServices'), $value);
        } elseif (is_string($value) &&  0 === strpos($value, '@=')) {
            return new Expression(substr($value, 2));
        } elseif (is_string($value) &&  0 === strpos($value, '@')) {
            if (0 === strpos($value, '@@')) {
                $value = substr($value, 1);
                $invalidBehavior = null;
            } elseif (0 === strpos($value, '@?')) {
                $value = substr($value, 2);
                $invalidBehavior = ContainerInterface::IGNORE_ON_INVALID_REFERENCE;
            } else {
                $value = substr($value, 1);
                $invalidBehavior = ContainerInterface::EXCEPTION_ON_INVALID_REFERENCE;
            }

            if ('=' === substr($value, -1)) {
                $value = substr($value, 0, -1);
                $strict = false;
            } else {
                $strict = true;
            }

            if (null !== $invalidBehavior) {
                $value = new Reference($value, $invalidBehavior, $strict);
            }
        }

        return $value;
    }
}