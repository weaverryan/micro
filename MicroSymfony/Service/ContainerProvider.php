<?php

namespace MicroSymfony\Service;

use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\Config\Loader\LoaderResolverInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;

class ContainerProvider implements LoaderInterface
{
    /**
     * @var ContainerBuilder
     */
    private $containerBuilder;

    /**
     * @var LoaderInterface
     */
    private $resourceLoader;

    public function __construct(ContainerBuilder $builder, LoaderInterface $resourceLoader)
    {
        $this->containerBuilder = $builder;
        $this->resourceLoader = $resourceLoader;
    }

    /**
     * @param string $serviceId
     * @param string $class
     * @param array $arguments
     * @return DefinitionBuilder
     */
    public function add($serviceId, $class = null, $arguments = array())
    {
        $definitionBuilder = DefinitionBuilder::create($class, $arguments);

        $this->containerBuilder->setDefinition($serviceId, $definitionBuilder->getDefinition());

        return $definitionBuilder;
    }

    /**
     * Create and add a service based on an existing template service
     *
     * @param string $serviceId
     * @param string $templateServiceId
     * @return DefinitionBuilder
     */
    public function addFromTemplate($serviceId, $templateServiceId)
    {
        $definitionBuilder = DefinitionBuilder::createFromTemplate($templateServiceId);

        $this->containerBuilder->setDefinition($serviceId, $definitionBuilder->getDefinition());

        return $definitionBuilder;
    }

    /**
     * @param string $serviceId
     * @param string $class
     * @param mixed $callable
     * @param array $arguments
     * @return DefinitionBuilder
     */
    public function addFromFactory($serviceId, $class, $callable, array $arguments = array())
    {
        $definitionBuilder = DefinitionBuilder::create($class, $arguments);
        $definitionBuilder->instantiateFromFactory($callable);

        $this->containerBuilder->setDefinition($serviceId, $definitionBuilder->getDefinition());

        return $definitionBuilder;
    }

    /**
     * Adds a new service "$serviceId" that is an alias to $toServiceId
     *
     *      // "main_logger" will really return the "logger" service
     *      $c->setAlias('main_logger', 'logger');
     *
     * @param string $serviceId
     * @param string $toServiceId
     */
    public function setAlias($serviceId, $toServiceId)
    {
        $this->containerBuilder->setAlias($serviceId, $toServiceId);
    }

    public function configureExtension($alias, array $configuration)
    {
        $this->containerBuilder->loadFromExtension($alias, $configuration);
    }

    public function getContainerBuilder()
    {
        return $this->containerBuilder;
    }

    /**
     * @see LoaderInterface
     */
    public function load($resource, $type = null)
    {
        return $this->resourceLoader->load($resource, $type);
    }

    /**
     * @see LoaderInterface
     */
    public function supports($resource, $type = null)
    {
        return $this->resourceLoader->supports($resource, $type);
    }

    /**
     * @see LoaderInterface
     */
    public function getResolver()
    {
        return $this->resourceLoader->getResolver();
    }

    /**
     * @see LoaderInterface
     */
    public function setResolver(LoaderResolverInterface $resolver)
    {
        return $this->resourceLoader->setResolver($resolver);
    }

    private function resolveServices($value)
    {
        if (is_array($value)) {
            return array_map(array($this, 'resolveServices'), $value);
        }

        if ($value instanceof Definition) {
            // a Definition is turned into a Reference
            return new Reference($value);
        }
    }
}