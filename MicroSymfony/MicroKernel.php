<?php

namespace MicroSymfony;

use MicroSymfony\Router\RouteProviderInterface;
use MicroSymfony\Router\RoutesBuilder;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Bundle\BundleInterface;
use Symfony\Component\HttpKernel\Kernel;
use MicroSymfony\Service\ContainerProvider;
use Dotenv\Dotenv;
use Symfony\Component\Debug\Debug;
use Symfony\Bundle\FrameworkBundle\FrameworkBundle;
use Symfony\Bundle\TwigBundle\TwigBundle;
use Doctrine\Common\Annotations\AnnotationRegistry;
use Symfony\Bundle\WebProfilerBundle\WebProfilerBundle;
use Symfony\Bundle\DebugBundle\DebugBundle;

abstract class MicroKernel extends Kernel implements RouteProviderInterface
{
    private $bundlesRegistered = false;

    /**
     * @var BundleInterface[]
     */
    private $bundlesToRegister = array();

    /**
     * An associative array of bundle names and their configuration arrays
     *
     * @var array
     */
    private $bundleConfiguration = array();

    /**
     * Call $this->addBundle() to add (and optionally configure) all bundles
     *
     * @return void
     */
    abstract protected function configureBundles();

    /**
     * Add any service definitions to your container
     *
     * @param ContainerProvider $c
     * @return void
     */
    abstract protected function configureServices(ContainerProvider $c);

    /**
     * Add any route definitions
     *
     * @param RoutesBuilder $routeBuilder
     * @return void
     */
    abstract protected function configureRoutes(RoutesBuilder $routeBuilder);

    /**
     * Call this before instantiating the Kernel
     *
     *      $loader = require_once __DIR__.'/vendor/autoload.php';
     *      AppKernel::configureAutoloader($loader);
     *
     * @param Callable $loader The autoloader callable
     */
    public static function configureAutoloader($loader)
    {
        AnnotationRegistry::registerLoader(array($loader, 'loadClass'));
    }

    public function __construct()
    {
        $rootDir = $this->getRootDir();

        if (file_exists($rootDir.'/.env')) {
            $dotenv = new Dotenv($rootDir, '.env');
            // need overload here, because putenv() seems to be sticky (??) when
            // using the built-in web server, and we need $_SERVER to always be set
            $dotenv->overload();
        }

        // todo - better exception message
        if (!isset($_SERVER['SYMFONY_ENV']) || !isset($_SERVER['SYMFONY_DEBUG'])) {
            throw new \LogicException(
                sprintf(
                    'The environment variables "SYMFONY_ENV" and "SYMFONY_DEBUG" both
                    need to be set. The most common way to set these environmental variables
                    is by creating a .env file at "%s" with SYMFONY_ENV="dev" and SYMFONY_DEBUG=1'
                , $rootDir
                )
            );
        }

        // fetch the environment and debug with defaults
        $env = $_SERVER['SYMFONY_ENV'];
        $debug = $_SERVER['SYMFONY_DEBUG'];

        if ($debug) {
            Debug::enable();
        }

        parent::__construct($env, $debug);
    }


    /**
     * Executes the request through the kernel and sends the response
     *
     * @param Request|null $request
     */
    public function run(Request $request = null)
    {
        if (null === $request) {
            $request = Request::createFromGlobals();
        }

        $response = $this->handle($request);
        $response->send();
        $this->terminate($request, $response);
    }

    /**
     * Adds a bundle to the kernel
     *
     * @param BundleInterface $bundle
     * @param array $configuration Configuration for this bundle
     * @return $this
     */
    public function addBundle(BundleInterface $bundle, $configuration = array())
    {
        if ($this->bundlesRegistered) {
            throw new \LogicException('addBundle() cannot be called after configureBundles() is called!');
        }

        $this->bundlesToRegister[$bundle->getName()] = $bundle;

        if ($configuration) {
            // add the configuration for the extension if any is given
            $this->bundleConfiguration[$bundle->getName()] = $configuration;
        }

        return $this;
    }

    /**
     * Configure a bundle after it's already been added
     *
     * @param string $name Like, FrameworkBundle
     * @param array $configuration
     */
    public function configureBundle($name, array $configuration)
    {
        if (!isset($this->bundlesToRegister[$name])) {
            // todo - give suggestions
            throw new \InvalidArgumentException(sprintf(
                'No bundle called "%s" has been registered.',
                $name
            ));
        }

        $current = isset($this->bundleConfiguration[$name]) ? $this->bundleConfiguration[$name] : array();
        // merge the new configuration on the old
        $this->bundleConfiguration[$name] = array_merge($current, $configuration);
    }

    /**
     * Call inside configureBundles() to add the most basic base bundles
     *
     * @return $this
     */
    protected function addBaseBundles()
    {
        return $this
            ->addBundle(new FrameworkBundle(), array(
                'secret' => '%secret%',
                'router' => array(
                    'resource' => 'kernel',
                    'type' => 'service'
                ),
                'templating' => array(
                    'engines' => array('twig')
                ),
                'profiler' => array(
                    'enabled' => '%kernel.debug%'
                )
        ))
            ->addBundle(new TwigBundle());
    }


    /**
     * Call this in configureBundles() to enable some debugging bundles
     * @return $this
     */
    protected function enableDebuggingBundles()
    {
        return $this
            ->addBundle(new WebProfilerBundle(), array(
                'toolbar' => true,
            ))
            ->addBundle(new DebugBundle());
    }

    public function registerBundles()
    {
        $this->addBaseBundles();

        $this->configureBundles();
        $this->bundlesRegistered = true;

        return array_values($this->bundlesToRegister);
    }

    public function buildRouting(RoutesBuilder $routeBuilder)
    {
        // add the debugging routes if the bundle is enabled
        $bundles = $this->getBundles();
        if (isset($bundles['WebProfilerBundle'])) {
            $routeBuilder->loadRoutes(
                '@WebProfilerBundle/Resources/config/routing/wdt.xml',
                '/_wdt'
            );

            $routeBuilder->loadRoutes(
                '@WebProfilerBundle/Resources/config/routing/profiler.xml',
                '/_profiler'
            );
        }

        $this->configureRoutes($routeBuilder);
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        /** @var ContainerProvider $loader */
        $kernelBuilder = $loader;

        foreach ($this->bundleConfiguration as $bundleName => $configuration) {
            $extension = $this->bundlesToRegister[$bundleName]->getContainerExtension();
            if (!$extension) {
                throw new \LogicException('Bundle "%s" does not have an extension that can be configured.');
            }

            $alias = $extension->getAlias();

            $kernelBuilder->configureExtension($alias, $configuration);
        }

        $this->configureServices($kernelBuilder);
    }

    public function getCacheDir()
    {
        return $this->rootDir.'/var/cache/'.$this->environment;
    }

    public function getLogDir()
    {
        return $this->rootDir.'/var/logs';
    }

    protected function getContainerLoader(ContainerInterface $container)
    {
        if (!$container instanceof ContainerBuilder) {
            throw new \LogicException('Only ContainerBuilder instances are supported.');
        }

        $loader = parent::getContainerLoader($container);

        return new ContainerProvider($container, $loader);
    }
}
