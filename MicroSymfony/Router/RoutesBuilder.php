<?php

namespace MicroSymfony\Router;

use Symfony\Component\Config\Resource\ResourceInterface;
use Symfony\Component\Routing\RouteCollection;

class RoutesBuilder
{
    /**
     * @var ServiceRouterLoader
     */
    private $serviceRouterLoader;

    private $collection;

    private $pendingRoutes = array();

    private $defaultRouteIndex = 0;

    public function __construct(ServiceRouterLoader $serviceRouterLoader)
    {
        $this->serviceRouterLoader = $serviceRouterLoader;

        $this->collection = new RouteCollection();
    }

    public function loadRoutes($resource, $prefix = null, $type = null)
    {
        /** @var RouteCollection $subCollection */
        $subCollection = $this->serviceRouterLoader->import($resource, $type);
        $subCollection->addPrefix($prefix);

        $this->pendingRoutes[] = $subCollection;

        // return the collection so more options can be added to it
        return $subCollection;
    }

    /**
     * @param string $path
     * @param string $controller
     * @return RouteBuilder
     */
    public function add($path, $controller)
    {
        $route = new RouteBuilder($path);
        $route->setController($controller);

        $this->pendingRoutes[] = $route;

        return $route;
    }

    public function compile()
    {
        foreach ($this->pendingRoutes as $routeOrCollection) {
            if ($routeOrCollection instanceof RouteCollection) {
                // add the collection
                $this->collection->addCollection($routeOrCollection);
            } elseif ($routeOrCollection instanceof RouteBuilder) {
                // add the single route
                if (!$name = $routeOrCollection->getName()) {
                    // auto-generate a route name
                    $name = $this->generateName($routeOrCollection->getPath());
                }

                $this->collection->add($name, $routeOrCollection);
            } else {
                throw new \LogicException(sprintf(
                    'Invalid route item "%s" given',
                    get_class($routeOrCollection)
                ));
            }
        }

        $this->pendingRoutes = array();
    }

    public function getCollection()
    {
        return $this->collection;
    }

    /**
     * Adds a resource for this collection.
     *
     * @param ResourceInterface $resource A resource instance
     */
    public function addResource(ResourceInterface $resource)
    {
        $this->collection->addResource($resource);
    }

    private function generateName($path)
    {
        $name = strtolower(str_replace(
            array('/', '.'),
            array('_', '_'),
            ltrim($path, '/'))
        );
        if ($this->defaultRouteIndex > 0) {
            $name .= '_'.$this->defaultRouteIndex;
        }
        ++$this->defaultRouteIndex;

        return $name;
    }
}
