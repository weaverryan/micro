<?php

namespace MicroSymfony\Router;

use Symfony\Component\Routing\Route;

class RouteBuilder extends Route
{
    private $name;

    public function setController($controller)
    {
        $this->setDefault('_controller', $controller);

        return $this;
    }

    public function setRequestFormat($format)
    {
        $this->setDefault('_format', $format);

        return $this;
    }

    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function getName()
    {
        return $this->name;
    }
}
