<?php
/**
 * Created by PhpStorm.
 * User: weaverryan
 * Date: 9/1/15
 * Time: 6:04 AM
 */

namespace MicroSymfony\Router;


interface RouteProviderInterface
{
    public function buildRouting(RoutesBuilder $routeBuilder);
}
