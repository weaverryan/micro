<?php

namespace MicroSymfony\Router;

use Symfony\Component\Config\Loader\FileLoader;
use Symfony\Component\Config\Loader\Loader;
use Symfony\Component\Config\Resource\FileResource;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Config\FileLocatorInterface;

class ServiceRouterLoader extends Loader
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Calls the service that will load the routes
     *
     * @param mixed $resource
     * @param null $type
     * @return \Symfony\Component\Routing\RouteCollection
     */
    public function load($resource, $type = null)
    {
        $service = $this->container->get($resource);

        if (!$service instanceof RouteProviderInterface) {
            throw new \LogicException(sprintf('Service "%s" must implement RouteProviderInterface.', $resource));
        }

        $routeBuilder = new RoutesBuilder($this);

        // make the service file tracked so that if it changes, the cache rebuilds
        $obj = new \ReflectionObject($service);
        $resource = new FileResource($obj->getFileName());
        $routeBuilder->addResource($resource);

        $service->buildRouting($routeBuilder);

        $routeBuilder->compile();

        return $routeBuilder->getCollection();
    }

    /**
     * Returns whether this class supports the given resource.
     *
     * @param mixed $resource A resource
     * @param string|null $type The resource type or null if unknown
     *
     * @return bool True if this class supports the given resource, false otherwise
     */
    public function supports($resource, $type = null)
    {
        return $type == 'service';
    }
}