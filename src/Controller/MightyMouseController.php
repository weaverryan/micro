<?php

namespace App\Controller;

use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class MightyMouseController extends ContainerAware
{
    /**
     * @Route("/")
     */
    public function rescueAction()
    {
        $generator = $this->container->get('quote_generator');
        $randomGenerator = $this->container->get('number_generator');

        $html = $this->container->get('cool_twig')->render(
            'mighty_mouse/rescue.html.twig',
            array('quote' => $generator->getQuote(), 'number' => $randomGenerator->random())
        );

        return new Response($html);
    }
}
