<?php

namespace App\Service;

use Psr\Log\LoggerInterface;

class Configurator
{
    private $logger;

    public function configureQuoteGenerator(QuoteGenerator $quoteGenerator)
    {
        $this->logger->info('configuring the quote generator');
    }

    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }
}
