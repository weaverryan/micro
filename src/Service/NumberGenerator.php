<?php

namespace App\Service;

class NumberGenerator
{
    private $min;
    private $max;

    public function __construct($min, $max)
    {
        $this->min = $min;
        $this->max = $max;
    }

    public function random()
    {
        return rand($this->min, $this->max);
    }
}