<?php

namespace App\Service;

use Psr\Log\LoggerInterface;

class QuoteGenerator
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function getQuote()
    {
        $quote = 'Here I come to save the day! '.rand();

        $this->logger->info($quote);

        return $quote;
    }

    public function createNumberGenerator($min, $max)
    {
        return new NumberGenerator($min, $max);
    }
}
