<?php

use MicroSymfony\MicroKernel;
use MicroSymfony\Service\ContainerProvider;
use Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle;
use Symfony\Bundle\MonologBundle\MonologBundle;
use MicroSymfony\Router\RoutesBuilder;

class AppKernel extends MicroKernel
{
    protected function configureBundles()
    {
        $this->addBaseBundles();

        if ($this->getEnvironment() == 'dev') {
            $this->enableDebuggingBundles();
        }

        $this
            ->addBundle(new SensioFrameworkExtraBundle())
            ->addBundle(new MonologBundle(), array(
                'handlers' => array(
                    'main' => array(
                        'type' =>         'fingers_crossed',
                        'action_level' => '%log_action_level%',
                        'handler' =>      'nested',
                    ),
                    'nested' => array(
                        'type' =>  'stream',
                        'path' =>  "%kernel.logs_dir%/%kernel.environment%.log",
                        'level' => 'debug',
                    )
                )
            ));

        // todo - not sure if I ultimately want this!
        $this->addBundle(new \App\AppBundle());
    }

    protected function configureRoutes(RoutesBuilder $routeBuilder)
    {
        $routeBuilder->loadRoutes(
            '@AppBundle/Controller',
            '/',
            'annotation'
        );

        $routeBuilder->add(
            '/api/number.json',
            'App\Controller\TraditionalController::returnJsonAction'
        )
            ->setName('my_api_endpoint')
            ->setRequestFormat('json');
    }

    protected function configureServices(ContainerProvider $c)
    {
        $c->add('app_configurator', 'App\Service\Configurator')
            ->addMethodCall('setLogger', array('@logger'));

        $c->add('quote_generator', 'App\Service\QuoteGenerator', array(
            '@logger'
            //$c->reference('logger')
        ))
            ->setConfigurator(array('@app_configurator', 'configureQuoteGenerator'))
        ;

        $c->addFromTemplate('cool_twig', 'twig')
            ->addMethodCall('enableDebug');

        // not ideal that the class is required for factories...
        $c->addFromFactory(
            'number_generator',
            'App\Service\NumberGenerator',
            array('@quote_generator', 'createNumberGenerator'),
            array(10, 20)
        );

        $c->add('my_request_listener', 'App\Service\RequestListener', array(
            '@logger'
        ))
            ->addEventListenerTag('kernel.request', 'onKernelRequest')
        ;

        $c->load(__DIR__.'/MicroSymfony/Resources/services.yml');

        // load some configuration!
        // technically redundant - just showing this off
        $c->configureExtension('framework', array(
            'profiler' => array(
                'enabled' => '%kernel.debug%'
            )
        ));
    }
}
