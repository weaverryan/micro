<?php
umask(0000);

$loader = require_once __DIR__.'/../vendor/autoload.php';
require_once __DIR__.'/../AppKernel.php';
AppKernel::configureAutoloader($loader);

$kernel = new AppKernel();
$kernel->run();
